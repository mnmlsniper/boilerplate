const urls = {
  httpService : 'https://httpbin.org',
  userService: 'http://localhost:8888',
  adminService: 'http://localhost:8081',
}

export {
  urls
}
