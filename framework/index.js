import {HttpService} from './services';

const serviceProvider = {
  // user: new UserService(),
  // admin: new AdminService(),
  http: new HttpService(),
}

export * from './config';
export {
  serviceProvider
}
