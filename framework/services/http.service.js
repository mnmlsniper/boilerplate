import {BaseInterface, decorateService} from '../../lib';
import {urls} from '../config'

class HttpService extends BaseInterface {
  constructor(host = urls.httpService) {
    super(host);
  }

  getIp() {
    return this.req.get({path: '/ip'});
  }
}
decorateService(HttpService);

export {
  HttpService
}
